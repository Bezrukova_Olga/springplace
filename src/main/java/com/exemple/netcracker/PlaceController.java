package com.exemple.netcracker;


import com.exemple.netcracker.converter.Convert;
import com.exemple.netcracker.domain.PlaceEntity;
import com.exemple.netcracker.domain.PlaceRequest;
import com.exemple.netcracker.domain.PlaceResponse;
import com.exemple.netcracker.repository.PlaceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/places")
public class PlaceController {
    @Autowired
    private PlaceRepo placeRepo;
    @Autowired
    private Convert converter;


    @RequestMapping(method = RequestMethod.POST)
    public PlaceResponse add(@RequestBody PlaceRequest place){
        PlaceEntity placeEntity = converter.toEntity(place);
        placeRepo.save(placeEntity);
        return converter.toResponse(placeEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PlaceResponse get(@PathVariable String  id){
        PlaceEntity found = placeRepo.findById(id).get();
        return converter.toResponse(found);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public HttpStatus delete(@PathVariable String id){
        placeRepo.deleteById(id);
        return HttpStatus.OK;
    }
}
