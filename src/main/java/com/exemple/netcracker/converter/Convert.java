package com.exemple.netcracker.converter;

import com.exemple.netcracker.domain.PlaceEntity;
import com.exemple.netcracker.domain.PlaceRequest;
import com.exemple.netcracker.domain.PlaceResponse;
import org.springframework.stereotype.Component;



@Component
public class Convert {
    public PlaceEntity toEntity(PlaceRequest place){
        return new PlaceEntity(place.getName(),place.getLatitude(), place.getLongitude());
    }

    public PlaceResponse toResponse(PlaceEntity place){
        return new PlaceResponse(place.getId(),place.getName(),place.getLatitude(), place.getLongitude());
    }
}
