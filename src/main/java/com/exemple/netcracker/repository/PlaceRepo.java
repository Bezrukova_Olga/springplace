package com.exemple.netcracker.repository;

import com.exemple.netcracker.domain.PlaceEntity;
import com.exemple.netcracker.domain.PlaceResponse;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PlaceRepo extends CrudRepository<PlaceEntity, String> {
    Optional<PlaceEntity> findById(String id);
    void deleteById(String id);
}
